//
//  ViewExtensions.swift
//  Challenge5
//
//  Created by Роман Хоменко on 26.06.2022.
//

import UIKit

extension UIView {
    func bounceOut(duration: Int) {
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: []) {
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        } completion: { _ in
            self.transform = .identity
        }
    }
}

extension Int {
    func times(closure: () -> Void) {
        for _ in 1...self {
            closure()
        }
    }
}

extension Array where Element: Comparable {
    mutating func remove(item: Element) {
        for index in 0..<count {
            if self[index] == item {
                remove(at: index)
                return
            }
        }
    }
}
