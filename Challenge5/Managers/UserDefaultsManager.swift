//
//  UserDefaultsManager.swift
//  Challenge5
//
//  Created by Роман Хоменко on 24.06.2022.
//

import Foundation

class UserDefaultsManager {
    static let shared = UserDefaultsManager()
    
    private let decoder = JSONDecoder()
    private let encoder = JSONEncoder()
    private let userDefaults = UserDefaults.standard
    
    func save<T: Codable>(data: T, with key: String) {
        let data = try? encoder.encode(data)
        userDefaults.set(data, forKey: key)
    }
    
    func load() throws -> [Note] {
        guard let data = userDefaults.data(forKey: arrayofNotesKey),
              let notes = try? decoder.decode([Note].self, from: data) else {
            throw Error.loadError
        }
        
        return notes
    }
    
    private enum Error: String, Swift.Error {
        case loadError
    }
    
    private init() {}
}
