//
//  Note.swift
//  Challenge5
//
//  Created by Роман Хоменко on 24.06.2022.
//

import Foundation

struct Note: Codable {
    var description: String?
    var id: String = UUID().uuidString
    
    init(description: String) {
        self.description = description
    }
}
