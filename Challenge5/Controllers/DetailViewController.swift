//
//  DetailViewController.swift
//  Challenge5
//
//  Created by Роман Хоменко on 24.06.2022.
//

import UIKit

protocol DetailViewControllerDelegate: AnyObject {
    func updateChangesInNote(note: Note, for id: String)
    func deleteNote(at index: String)
}

class DetailViewController: UIViewController {
    
    // Properties
    var note: Note?
    weak var delegate: DetailViewControllerDelegate?
    
    var userDefaults = UserDefaultsManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setupViewWithConstraints()
    }
}

extension DetailViewController {
    func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        
        let deleteBarItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteCurrentNote))
        let shareBarItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareNote))
        
        navigationItem.rightBarButtonItems = [deleteBarItem, shareBarItem]
        
        title = "Detail Note"
    }
}

// MARK: - Setup view elements with constraints
extension DetailViewController {
    func setupViewWithConstraints() {
        lazy var detailDescriptionTextView: UITextView = {
            let textView = UITextView()
            textView.translatesAutoresizingMaskIntoConstraints = false
            textView.text = "\(note?.description != nil ? note!.description! : "There is no text in Note")"
            textView.font = UIFont.preferredFont(forTextStyle: .footnote)
            textView.font = UIFont.systemFont(ofSize: 20)
            
            textView.delegate = self
            
            return textView
        }()
        
        self.view.addSubview(detailDescriptionTextView)
        
        detailDescriptionTextView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        detailDescriptionTextView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        detailDescriptionTextView.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        detailDescriptionTextView.heightAnchor.constraint(equalToConstant: view.frame.size.height).isActive = true
        
        detailDescriptionTextView.bounceOut(duration: 2)
    }
}

// MARK: - UITextView methods
extension DetailViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        guard var note = note else { return }
        note.description = textView.text
        
        delegate?.updateChangesInNote(note: note, for: note.id)
    }
}

// MARK: - @objc methods
extension DetailViewController {
    @objc func deleteCurrentNote() {
        delegate?.deleteNote(at: note!.id)
        
        navigationController?.popViewController(animated: true)
    }
    
    @objc func shareNote() {
        guard let note = note?.description else {
            print("There is no description")
            return
        }
        let activityVC = UIActivityViewController(activityItems: [note], applicationActivities: [])
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityVC, animated: true)
    }
}
