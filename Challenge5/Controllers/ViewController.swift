//
//  ViewController.swift
//  Challenge5
//
//  Created by Роман Хоменко on 24.06.2022.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Propertis
    private var tableView: UITableView!
    var notes: [Note] = []
    let userDefaults = UserDefaultsManager.shared
    
    var delegate: DetailViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupNavigationBar()
        setupTableView()
        
//        10.times {
//            print("Hello!!")
//        }
        var arr = [1, 2, 3, 4, 5, 6, 7, 8, 99, 100, 101]
        arr.remove(item: 6)
        print(arr)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let loadedNotes = try? userDefaults.load() {
            notes = loadedNotes
            tableView.reloadData()
        }
    }
}

// MARK: - setup tableView
extension ViewController {
    func setupTableView() {
        tableView = UITableView(frame: view.frame)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
    }
}

// MARK: - setup navigationBar
extension ViewController {
    func setupNavigationBar() {
        title = "My challenge 5"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(newNote))
    }
}

// MARK: - Add DataSource, Delegate methods and TableView methods
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath)
        cell.textLabel?.text = notes[indexPath.row].description
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        goToDetailVC(noteIndex: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notes.remove(at: indexPath.row)
            
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            userDefaults.save(data: notes, with: arrayofNotesKey)
        }
    }
}

// MARK: - Helper methods
extension ViewController {
    func goToDetailVC(noteIndex: Int) {
        guard let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return }
        detailVC.delegate = self
        
        detailVC.note = notes[noteIndex]
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

// MARK: - @objc methods
extension ViewController {
    @objc func newNote() {
        let alert = UIAlertController(title: "New Note",
                                      message: "Add some text",
                                      preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "Future note..."
        }
        alert.addAction(UIAlertAction(title: "Add",
                                      style: .default) { [weak alert, weak self] _ in
            let note = Note(description: alert?.textFields?.first?.text ?? "")
            
            self?.notes.append(note)
            
            // save all notes after appending one
            self?.userDefaults.save(data: self?.notes, with: arrayofNotesKey)
            self?.tableView.reloadData()
        })
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: .cancel))
        present(alert, animated: true)
    }
}

// MARK: - Protocol Delegate Methods
extension ViewController: DetailViewControllerDelegate {
    func updateChangesInNote(note: Note, for id: String) {
        
        for i in 0..<notes.count {
            if notes[i].id == note.id {
                notes[i] = note
                // save changes in User Defaults
                userDefaults.save(data: notes, with: arrayofNotesKey)
                notes = try! userDefaults.load()
                
                tableView.reloadData()
            }
        }
    }
    
    func deleteNote(at index: String) {
        let currentIndex = Int(index) ?? 0
        notes.remove(at: currentIndex)
        
        tableView.reloadData()
        
        userDefaults.save(data: notes, with: arrayofNotesKey)
    }
}
